package com.brainacad.oop.testjunit.testcase;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.brainacad.oop.testjunit.testjunit.TemperatureConverter;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collection;

/**
 * 
 General TestClass Simple
 http://www.rapidtables.com/convert/temperature/how-fahrenheit-to-celsius.htm
 
 */
@RunWith(Parameterized.class)
public class TemperatureConverterC2FTest {
    TemperatureConverter temperatureConverter;
    double degreesFahrenheit;
    double degreesCelsius;
    double kelvin;
    double delta;

    // порядок параметрів конструктора співпадає з порядком елементів масиву C / F
    public TemperatureConverterC2FTest(double  degreesCelsius, double degreesFahrenheit) {
        this.degreesFahrenheit = degreesFahrenheit;
        this.degreesCelsius = degreesCelsius;
    }

    @Before
    public void setUp() throws Exception {
        temperatureConverter = new TemperatureConverter();
        delta = 0.01;
    }

    @After
    public void tearDown() throws Exception {
        temperatureConverter = null;
    }

    @Parameterized.Parameters
    public static Collection tableC2F() {
        return Arrays.asList(new Object[][] {
        //Celsius (°C),  Fahrenheit (°F)        
            {-273.15,   -459.67 }   ,   // absolute zero temperature
            {   -50.0 ,   -58.0   }   ,
            {   -40.0 ,   -40.0   }   ,
            {   -30.0 ,   -22.0   }   ,
            {   -20.0 ,   -4.0    }   ,
            {   -10.0 ,   14.0    }   ,
            {   -9.0  ,   15.8    }   ,
            {   -8.0  ,   17.6    }   ,
            {   -7.0  ,   19.4    }   ,
            {   -6.0  ,   21.2    }   ,
            {   -5.0  ,   23.0    }   ,
            {   -4.0  ,   24.8    }   ,
            {   -3.0  ,   26.6    }   ,
            {   -2.0  ,   28.4    }   ,
            {   -1.0  ,   30.2    }   ,
            {   0.0   ,   32.0    }   ,   // freezing/melting point of water
            {   1.0   ,   33.8    }   ,
            {   2.0   ,   35.6    }   ,
            {   3.0   ,   37.4    }   ,
            {   4.0   ,   39.2    }   ,
            {   5.0   ,   41.0    }   ,
            {   6.0   ,   42.8    }   ,
            {   7.0   ,   44.6    }   ,
            {   8.0   ,   46.4    }   ,
            {   9.0   ,   48.2    }   ,
            {   10.0  ,   50.0    }   ,
            {   20.0  ,   68.0    }   ,
            {   21.0  ,   69.8    }   ,   // room temperature
            {   30.0  ,   86.0    }   ,
            {   37.0  ,   98.6    }   ,   // average body temperature
            {   40.0  ,   104.0   }   ,
            {   50.0  ,   122.0   }   ,
            {   60.0  ,   140.0   }   ,
            {   70.0  ,   158.0   }   ,
            {   80.0  ,   176.0   }   ,
            {   90.0  ,   194.0   }   ,
            {   100.0 ,   212.0   }   ,   // boiling point of water
            {   200.0 ,   392.0   }   ,
            {   300.0 ,   572.0   }   ,
            {   400.0 ,   752.0   }   ,
            {   500.0 ,   932.0   }   ,
            {   600.0 ,   1112.0  }   ,
            {   700.0 ,   1292.0  }   ,
            {   800.0 ,   1472.0  }   ,
            {   900.0 ,   1652.0  }   ,
            {   1000.0,   1832.0  }
        });
        
    }

    @Test
    public void convertCtoF() throws Exception {
        double actual = temperatureConverter.convertCtoF(degreesCelsius);
        assertEquals("C2F incorrect ", degreesFahrenheit, actual, delta);
    }
}